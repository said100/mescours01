# Présentation du playground

>plateforme utilisé pour s'exercer avec javascript

- Récupérer repo tryHard dans gitlab => clone => copier l'url
- Dans webstorm => vcs => get from version control => coller l'url => clone
- Dans le dossier tryHard => double cliqué sur package.json
- Passer sur les éléments surligné, cliquer sur run npm install
>ou aller dans la console, dans dossier tryHard taper : npm i
- Le programme s'installe qui va gérer le live reload qui permet de recharger le js sans rafraichir pour voir le résultat
- Sur package.json, clique droit => show npm scripts
- Double cliqué sur serve, ça va lancer un serveur
- Remonter dans la console jusqu'a trouver localhost, on clique
- Une fenêtre s'ouvre, clique droit, inspecter puis console
- Dans le playground écrire le js, le résultat s'affiche dans la console

