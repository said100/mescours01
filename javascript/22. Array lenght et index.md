# Array et index

- le premier élément d'un tablaeu est index 0

      `console.log(tab[0])`
      
- le deuxième élément d'un tablaeu est index 1

      `console.log(tab[1])`
      
- le troisième élément d'un tablaeu est index 2

      `console.log(tab[2])`

# Array lenght
      
- si on veut savoir la longueur d'un tableau :

       `console.log(tab.lenght);`
       
- si on veut savoir le dernier élément d'un tableau :

       `console.log(tab[tab.lenght - 1]);`