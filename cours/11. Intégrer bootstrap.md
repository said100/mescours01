# Intégrer bootstrap

> Apprendre à utiliser les components (un outil) qui sont fourni avec bootstrap .

## Un component

> un outil de html fait pour faciliter la tache des développeurs avec toute une librairie prête à l'emploi

## Utiliser un component

- Trouver un component dans bootstrap
- Ne pas oublier de mettre le lien bootstrap dans le head

    ```
    <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.css">
    ```
- Ne pas oublier de mettre le lien styles.css dans le head
- Ne pas oublier de mettre le lien js dans le head :

    ```
    <script src="../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    ```
- Ne pas oublier d'installer jquery par le terminal : `npm install jquery` OK
- Ne pas oublier de mettre jquery dans le head :

    ```jquery
    <script src="../jquery/dist/js/jquery.min"></script>
    ```      
> mettre jquery avant bootstrap, parce que bootstrap à besoin de jquery pour fonctionner