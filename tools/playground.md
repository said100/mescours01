# Présentation du playground

>Le playground l'endroit ou on va pouvoir jouer avec javascript
>
- Dans gitlab aller dans le repo tryHard 
 
- Gitlab => clone => copier l'url

- Dans webstorm => vcs => get from version control

- Get from version control => coller l'url => clone 

- Ouvrir package.json

- Cliquer sur Run npm.install en passant la souris 

    - ou aller dans la console dans le dossier tryHard taper : `npm i`
    
- Live Reload est est installé

- clique droit sur package.json => double cliqué serve(lance le serveur)

- Remonter dans la console jusqu'à `http://localhost:3000` cliqué ça ouvre une fenêtre

- ouvrir la console 

- Aller dans le fichier Playground => utiliser javascript